package org.aliciafdezrov.EjercicioMocking;

import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import org.junit.Test;

public class OrdenDeCompraTest {

	@Test
	public void pruebaHayArticulos(){
    
	//Creating de mock object
	Almacen almacen = mock(Almacen.class);
	
	//Stubbing
	
	when(almacen.hayArticulos("Huevos", 12)).thenReturn(true);
	when(almacen.eliminarArticulo("Huevos", 12)).thenReturn(true);
	
	//Executing
	OrdenDeCompra orden = new OrdenDeCompra("Huevos", 12);
	orden.efectuarCompra(almacen);
	
	//Verifying 
	
	verify(almacen).hayArticulos("Huevos", 12);
	verify(almacen).eliminarArticulo("Huevos", 12);
	}
	
}
