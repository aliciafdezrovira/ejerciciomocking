package org.aliciafdezrov.EjercicioMocking;


import org.junit.Test;
import java.util.List;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;


public class pruebaListMockito {

	@Test
	  public void pruebaLista() {
	    // Creating the mock object
	    List<String> list = mock(List.class) ;

	    // Stubbing: defining the behavior
	    when(list.get(0)).thenReturn("First") ;
	    when(list.get(1)).thenReturn("Second") ;
	    when(list.get(3)).thenReturn("Third");
	    

	    // Using the mock object
	    System.out.println(list.get(0));
	    System.out.println(list.get(1));
	    System.out.println(list.get(3));

	    // Verifying
	    verify(list, times(1)).get(0) ;
	    verify(list, times(1)).get(1);
	    verify(list, times(1)).get(3);
	  }
	
	@Test (expected = RuntimeException.class)
	  public void shouldGetTwoReturnAnException(){
		  
		  //Creating the mock object
		  List<String> list = mock(List.class) ;
		  
		  //Stubbing: defining the behavior 	  	  
		  when(list.get(2));
		  
		  //Using the mock object
		  
		  System.out.println(list.get(2));
		  
		  //Verify
		  
		  verify(list, times(0)).get(2);
		   
	  }
}
