package org.aliciafdezrov.EjercicioMocking;


public class OrdenDeCompra {

	private String nombreDelProducto ;
	  private Integer cantidad ;

	  public OrdenDeCompra(String nombre, int cantidad) {
	    nombreDelProducto = nombre ;
	    this.cantidad = cantidad ;
	  }

	  public void efectuarCompra(Almacen almacen) {
	    if (almacen.hayArticulos(nombreDelProducto, cantidad)) {
	      almacen.eliminarArticulo(nombreDelProducto, cantidad);
	    }
	  }

	  public Integer getCantidad() {
	    return cantidad;
	  }
}
