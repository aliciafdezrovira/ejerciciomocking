package org.aliciafdezrov.EjercicioMocking;

public interface Almacen {
	boolean eliminarArticulo(String articulo, int cantidad) ;
	boolean hayArticulos(String articulo, int cantidad) ;
}
